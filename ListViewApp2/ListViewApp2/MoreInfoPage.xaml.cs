﻿using ListViewApp2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ListViewApp2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MoreInfoPage : ContentPage
	{
        public MoreInfoPage()
        {
            InitializeComponent();
        }

        public MoreInfoPage(ProductData data)
        {
            InitializeComponent();

            BindingContext = data;
        }
    }
}