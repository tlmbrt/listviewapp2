﻿using ListViewApp2.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using System.Reflection;
using System.IO;

namespace ListViewApp2
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<ProductData> data = new ObservableCollection<ProductData>();

        public MainPage()
        {
            InitializeComponent();
            this.Title = "Marketplace";
            ReadInJsonFile();
        }

        private void ReadInJsonFile()
        {
            var fileName = "ListViewApp2.products.json";
            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(fileName);

            using (var reader = new System.IO.StreamReader(stream))
            {
                var jsonAsString = reader.ReadToEnd();
                data = JsonConvert.DeserializeObject<ObservableCollection<ProductData>>(jsonAsString);
            }
            productList.ItemsSource = data;
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var itemClicked = menuItem.CommandParameter as ProductData;
            await Navigation.PushAsync(new MoreInfoPage(itemClicked));
        }

        private void handleRefresh(object sender, System.EventArgs e)
        {
            ListView view = (ListView)sender;

            view.IsRefreshing = false;
        }
    }
}
