﻿using System;
using System.Collections.Generic;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ListViewApp2.Models
{
    public class ProductData
    {
        [JsonProperty("productId")]
        public int productId { get; set; }

        [JsonProperty("productName")]
        public string productName { get; set; }

        [JsonProperty("productCode")]
        public string productCode { get; set; }

        [JsonProperty("releaseDate")]
        public string releaseDate { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }

        [JsonProperty("price")]
        public float price { get; set; }

        [JsonProperty("starRating")]
        public float starRating { get; set; }

        [JsonProperty("imageUrl")]
        public string imageUrl { get; set; }
    }
}
